-- MySQL dump 10.13  Distrib 5.5.22, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ai_shop
-- ------------------------------------------------------
-- Server version	5.5.22-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachments`
--

LOCK TABLES `attachments` WRITE;
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
INSERT INTO `attachments` VALUES (1,'static/img/attachments/php-activerecord--model-validate-with-form.png','2012-05-08 18:32:10','2012-05-08 18:32:10'),(4,'static/img/attachments/ai_shop_Models.png','2012-05-08 18:34:26','2012-05-08 18:34:26'),(5,'static/img/attachments/6.jpg','2012-05-09 23:38:11','2012-05-09 23:38:11');
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `weight` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'category 1',0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password_hash` varchar(40) NOT NULL,
  `role` enum('user','admin') NOT NULL DEFAULT 'user',
  `status` enum('active','blocked') NOT NULL DEFAULT 'active',
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthdate` date NOT NULL,
  `id_card` varchar(13) NOT NULL,
  `address` text NOT NULL,
  `province` varchar(50) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `phone_tel` varchar(20) NOT NULL,
  `phone_mobile` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'admin@example.com','471b9f8dc04fc6981bdc5c15209d9857c6be27df','admin','active','T','K','male','1986-06-10','1101900567890','1010 Digital Rd.','Bangkok','10101','020202020','0880880800','2012-05-07 03:46:00','2012-05-07 16:59:31');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `weight` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Home','',0),(2,'About','about',1),(3,'Products','products',0);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `detail` mediumtext,
  `status` enum('available','sold_out','pre_order','promotion') NOT NULL DEFAULT 'available',
  `price` mediumint(9) NOT NULL,
  `price_sale` mediumint(9) NOT NULL DEFAULT '0',
  `stock` mediumint(9) NOT NULL DEFAULT '0',
  `stock_sold` mediumint(9) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `attachment_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'test 11111','<p>ทดสอบนะจ๊ะ</p>','available',58,0,100000,1,'2012-05-06 06:04:38','2012-05-09 23:39:27',1,4),(2,'006-18 Fantastic Summer - Cutie Cat Stripe Tee - มี 2 สี','<div>กรี๊สสสมากกับรายการนี้ น่ารักสุดๆๆเลยคร่ากับเสื้อลายขวาง กุ๊นขอบชมพู มีกระเป๋าใบเล็กๆด้านหน้าปักรูปแมวน้อยน่ารักๆๆ เนื้อผ้า cotton ยืด เนื้อนิ่มใส่สบายมาก มีไว้ไม่ผิดหวังแน่นอนจ้า<br /><br /></div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<div><strong>หมายเหตุ:</strong>\n<div>สินค้ามี 2 สี ครีมและขาว ญแย้ใส่สีครีม</div>\n</div>\n<p>&nbsp;</p>\n<table border=\"0\" cellspacing=\"1\" cellpadding=\"5\" bgcolor=\"#CECECE\">\n<tbody>\n<tr bgcolor=\"#fbf8f5\">\n<td align=\"center\" width=\"80\" height=\"30\">Size</td>\n<td align=\"center\" width=\"80\">ยาว</td>\n<td align=\"center\" width=\"80\">อก</td>\n<td align=\"center\" width=\"80\">บ่ากว้าง</td>\n<td align=\"center\" width=\"80\">แขนยาว</td>\n<td align=\"center\" width=\"80\">รอบต้นแขน</td>\n<td align=\"center\" width=\"80\">เอว</td>\n<td align=\"center\" width=\"80\">เอวต่ำ</td>\n<td align=\"center\" width=\"80\">สะโพก</td>\n<td align=\"center\" width=\"80\">เป้ายาว</td>\n</tr>\n<tr bgcolor=\"#FFFFFF\">\n<td align=\"center\" height=\"27\">-</td>\n<td align=\"center\">25</td>\n<td align=\"center\">33-37</td>\n<td align=\"center\">14</td>\n<td align=\"center\">8</td>\n<td align=\"center\">รอบปลายแขน 11-13</td>\n<td align=\"center\">วงแขน 16</td>\n<td align=\"center\">ชายเสื้อ 36</td>\n<td align=\"center\">-</td>\n<td align=\"center\">-</td>\n</tr>\n</tbody>\n</table>\n<p><br /><br /><img src=\"http://morning-kiss.com/upload/1625/lot18_006_01.jpg\" alt=\"\" /><br /><img src=\"http://morning-kiss.com/upload/1625/lot18_006_02.jpg\" alt=\"\" /><br /><img src=\"http://morning-kiss.com/upload/1625/lot18_006_03.jpg\" alt=\"\" /><br /><img src=\"http://morning-kiss.com/upload/1625/lot18_006_04.jpg\" alt=\"\" /><br /><img src=\"http://morning-kiss.com/upload/1625/lot18_006_05.jpg\" alt=\"\" /><br /><img src=\"http://morning-kiss.com/upload/1625/006_01.jpg\" alt=\"\" /><br /><br /><img src=\"http://morning-kiss.com/upload/1625/006_02.jpg\" alt=\"\" /><br /><br /><img src=\"http://morning-kiss.com/upload/1625/006_03.jpg\" alt=\"\" /><br /><br /><img src=\"http://morning-kiss.com/upload/1625/006_04.jpg\" alt=\"\" /><br /><br /><img src=\"http://morning-kiss.com/upload/1625/006_05.jpg\" alt=\"\" /><br /><br /><br /><br /><br /></p>\n<div>&nbsp;</div>\n<p><img src=\"http://www.morning-kiss.com/images/model-size_3.jpg\" alt=\"\" /></p>','pre_order',350,0,30,16,'2012-05-09 23:38:11','2012-05-09 23:38:11',1,5);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-05-10  6:58:12
