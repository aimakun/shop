<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @file MY_Controller.php
 *
 * Note for developers:
 * This class create to 'Extending Native Libraries' as:
 * http://codeigniter.com/user_guide/general/creating_libraries.html
 * As we see, this class extends CI Controller.
 *
 * This class create autoload global output variables,
 * which be used for page template (frontend_page view.)
 * 
 * See: frontend_page.php (view) and various controllers in this project.
 */
class MY_Controller extends CI_Controller {
    private $_view_output;
    function __construct()
    {
        parent::__construct();
    }
    
    function init_frontend_page($page_title = '', $show_my_orders = TRUE)
    {
        // Track user referrer path, this wouldn't be updated until next request.
        $this->session->set_flashdata('referrer', current_url());
        
        // Frontend template: initialize $output for fail-safe.
        $this->_view_output = array();
        
        // Frontend template: add page title.
        $this->_view_output['page_title'] = $page_title;
        
        // Menu access conditions.
        if ($this->session->userdata('email'))
        {
            $menu_conditions = "access IN ('all', 'member')";
        }
        else
        {
            $menu_conditions = "access IN ('all', 'anonymous')";
        }
        
        // Frontend template: render main menu.
        $this->_view_output['menu'] = $this->load->view('frontend/menu', 
            array('items' => Menu::find(
                    'all', 
                    array(
                        'conditions' => $menu_conditions,
                        'order' => 'weight'
                    )
                )
            )
        , TRUE);
        
        // Frontend template: render sidebar menu.
        $this->_view_output['sidebar_menu'] = $this->load->view('frontend/sidebar_menu', 
            array('items' => Category::find(
                    'all', 
                    array('order' => 'weight')
                )
            )
        , TRUE);
        
        // Frontend template: render member blocks.
        
        // Show current user's orders.
        // Anonymous store in session, otherwise load in Order model.
        
        $member = FALSE; // Send bool(FALSE) to notify menu_bar view for anonymous access.
        
        $member = Member::get_current_profile($authenticate = FALSE);
        
        
        $orders = Order::get_current_orders();
        
        if ($show_my_orders)
        {
            $orders_list = $this->load->view('orders/waiting_list',
                array(
                    'orders' => $orders,
                )
            , TRUE);
        }
        else
        {
            $orders_list = '';
        }
        
        $this->_view_output['members_block'] = $this->load->view('members/frontend_block.php',
            array(
                'member' => $member,
                'orders_list' => $orders_list,
            )
        , TRUE);
        
        // Frontend template: render footer blocks.
        $this->_view_output['footer'] = $this->load->view('frontend/footer', '', TRUE);
        return $this->_view_output;
    }
    
    function init_backend_page($page_title = '')
    {
        // Backend template: initialize $output for fail-safe.
        $this->_view_output = array();
        
        // Backend template: add page title.
        $this->_view_output['page_title'] = $page_title;
        
        // Backend template: render admin menu.
        $admin_menu = array(
            (object) array('link' => 'admin/dashboard', 'title' => 'Dashboard'),
            (object) array('link' => 'admin/products', 'title' => 'Products'),
            (object) array('link' => 'admin/categories', 'title' => 'Categories'),
            (object) array('link' => 'admin/reports', 'title' => 'Reports'),
        );
        $this->_view_output['admin_menu'] = $this->load->view('backend/admin_menu', 
            array(
                'items' => $admin_menu,
            )
        , TRUE);
        
        // Backend template: render main menu.
        /*
        $this->_view_output['menu'] = $this->load->view('backend/menu', 
            array('items' => Menu::find(
                    'all', 
                    array('order' => 'weight')
                )
            )
        , TRUE);
        */
        
        // Backend template: render sidebar menu.
        $this->_view_output['sidebar_menu'] = $this->load->view('backend/sidebar_menu', 
            array('items' => Category::find(
                    'all', 
                    array('order' => 'weight')
                )
            )
        , TRUE);
        
        // Backend template: render footer blocks.
        $this->_view_output['footer'] = $this->load->view('backend/footer', '', TRUE);
        return $this->_view_output;
    }
}
