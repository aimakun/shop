<?php
class Bill extends ActiveRecord\Model {
    public function create_form($bill)
    {
        $member = Member::get_current_profile();
        $provinces = $member->get_provinces();
        $transfer_date = explode('-', date('Y-n-j'));
        $transfer_date = array(
            'year' => $transfer_date[0],
            'month' => $transfer_date[1],
            'day' => $transfer_date[2]
        );
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
        $this->form_validation->set_rules('bank', 'Bank', 'required|callback_validate_bank');
        $this->form_validation->set_rules('transfer_day', 'Transfer date (day)', 'callback_validate_transfer_date');
        $this->form_validation->set_rules('shipping_type', 'Shipping type', 'required|callback_validate_shipping_type');
        $this->form_validation->set_rules('firstname', 'First name', 'required');
        $this->form_validation->set_rules('lastname', 'Last name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('province', 'Province', 'callback_validate_province');
        $this->form_validation->set_rules('zipcode', 'ID Zip code', 'required|numeric|exact_length[5]');
        
        if ($this->form_validation->run())
        {
            // Save bill and wait for administrator to check them.
            $transfer_day = str_pad((int) $this->input->post('transfer_day'), 2, '0', STR_PAD_LEFT);
            $transfer_month = str_pad((int) $this->input->post('transfer_month'), 2, '0', STR_PAD_LEFT);
            $transfer_year = str_pad((int) $this->input->post('transfer_year'), 4, '0', STR_PAD_LEFT);
            
            $bill->transfer_date = $transfer_day . '-' . $transfer_month . '-' . $transfer_year;
            $bill->transfer_time = date('H:i', strtotime($this->input->post('transfer_time')));
            $bill->amount = $this->input->post('amount');
            $bill->bank = $this->input->post('bank');
            $bill->shipping_type = $this->input->post('shipping_type');
            $bill->shipping_address = 
                $this->input->post('firstname') . ' ' . $this->input->post('lastname') . "\n" .
                $this->input->post('address') . "\n" .
                $provinces[$this->input->post('province')] . "\n" .
                $this->input->post('zipcode') . "\n";
            $bill->member_id = $member->id;
            $bill->save();
            
            // Update all current orders to billing state.
            $orders = Order::get_current_orders();
            foreach ($orders as $updated_order)
            {
                $updated_order->bill_id = $bill->id;
                $updated_order->status = 'notify';
                $updated_order->save();
            }
            
            $this->session->set_flashdata('status_message', array('success' => 'ท่านได้แจ้งชำระเงินเรียบร้อยแล้ว'));
            redirect('orders/bill');
        }
        
        return $this->load->view('bills/edit',
            array(
                'bill' => $bill,
                'member' => $member,
                'banks' => array_merge(array('-- Please select --'), Bill::get_enum_values('bank')),
                'provinces' => $provinces,
                'transfer_date' => $transfer_date,
            )
        , TRUE);
    }
    
    public function get_enum_values($field)
    {
        $query = $this->db->query("SHOW COLUMNS FROM bills LIKE '$field'");
        $field_attributes = $query->result();
        
        if (strpos($field_attributes[0]->Type, 'enum') !== 0)
        {
            return FALSE;
        }
        
        // The field type format from MySQL is:
        // [Type] => enum('available','sold_out','pre_order','promotion')
        // So we strip string and cut them to array with substr() and explode() function.
        
        $enum_values = substr($field_attributes[0]->Type, 6, -2);
        $enum_values = explode("','", $enum_values);
        
        return $enum_values;
    }
}
