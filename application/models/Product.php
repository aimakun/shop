<?php
class Product extends ActiveRecord\Model {
    static $belongs_to = array(
        array('category'),
        array('attachment')
    );
    
    static $has_many = array(
        array('attribute')
    );
}
