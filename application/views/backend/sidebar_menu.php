<?php
/**
 * @file sidebar_menu.php
 *
 * Template view for the sidebar menu.
 *
 * Available variables:
 * $items: menu items array contains some properties:
 * -- link: relative path (except static directory.)
 * -- title: human-readable text for the link.
 * -- weight: this value is used for sort menu items.
 * 
 * Note for developers:
 * Collect menu items with foreach() loop and create output as example below:
 * foreach ($items as $menu_item):
 *   print '<a href="' . $menu_item->link . '">' . $menu_item->title . '</a>';
 * endforeach;
 * 
 */
?>
