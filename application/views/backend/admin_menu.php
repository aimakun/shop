<?php
/**
 * @file sidebar_menu.php
 *
 * Template view for the sidebar menu.
 *
 * Available variables:
 * $items: menu items array contains some properties:
 * -- link: relative path (except static directory.)
 * -- title: human-readable text for the link.
 * -- weight: this value is used for sort menu items.
 * 
 * Note for developers:
 * Collect menu items with foreach() loop and create output as example below:
 * foreach ($items as $menu_item):
 *   print '<a href="' . $menu_item->link . '">' . $menu_item->title . '</a>';
 * endforeach;
 * 
 */
?>
<nav class="admin-bar navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <?php print anchor(base_url(), 'aiShop', array('class' => 'brand')); ?>
            <div class="navbar-text pull-right">
                Welcome, 
                <?php 
                print $this->session->userdata('name') . 
                    ' (' . $this->session->userdata('email') .')';
                ?>.
            </div>
            <div class="nav-collapse">
                <ul class="nav">
                <?php
                foreach ($items as $menu_item):
                    $url = base_url($menu_item->link);
                ?>
                    <li<?php print $menu_item->link == uri_string(current_url()) ? ' class="active"' : ''; ?>>
                        <?php print anchor($url, $menu_item->title); ?>
                    </li>
                <?php endforeach; ?>
                    <li class="divider-vertical"></li>
                    <li><?php print anchor('members/logout', 'Logout'); ?></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
