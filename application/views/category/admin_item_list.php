<?php
/**
 * @file admin_item_list.php
 *
 * Category template view for item listing (backend).
 *
 * Available variables:
 * $items: category items array contains some properties:
 * -- id: category id which refer to unique product.
 * -- title: category name.
 * -- weight: use for category ordering.
 * 
 * Note for developers:
 * Collect category items with foreach() loop and create output as example below:
 * foreach ($items as $category_item):
 *   print '<a href="/admin/edit/category/' . $category_item->id . '">' . $category_item->title . '</a>';
 * endforeach;
 * 
 */
?>
<a href="<?php print base_url('admin/add/category'); ?>" class="btn btn-large btn-primary"><i class="icon-plus icon-white"></i> New category</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Category</th>
            <th>Weight</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $category_item): ?>
        <tr>
            <td><?php print $category_item->id; ?></td>
            <td><i class="icon-tag"></i> <?php print $category_item->title; ?></td>
            <td><?php print $category_item->weight; ?></td>
            <td>
                <?php if ($category_item->id == 1): ?>
                    (Default) | 
                <?php else: ?>
                    <a href="<?php print base_url('admin/delete/category/' . $category_item->id); ?>" onclick="return confirm('Are you sure to delete?');"><i class="icon-trash"></i> Delete</a> | 
                <?php endif; ?>
                <a href="<?php print base_url('admin/edit/category/' . $category_item->id); ?>"><i class="icon-edit"></i> Edit</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
