<?php
/**
 * @file admin_edit.php
 *
 * Backend - category edit form.
 *
 * Available variables:
 * $category: category item contains some properties:
 * -- id: category id which refer to unique product.
 * -- title: category name.
 * -- weight: use for category ordering.
 * 
 */

$error = validation_errors();
if (!empty($error)): ?>
<div class="alert alert-error">
    <?php print $error; ?>
</div>
<?php endif; ?>
<?php print form_open(current_url(), array('class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="title">Title</label>
    <div class="controls">
        <input type="text" id="title" name="title" value="<?php print set_value('title', $category->title); ?>" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="weight">Weight</label>
    <div class="controls">
        <input type="text" id="weight" name="weight" value="<?php print set_value('weight', $category->weight); ?>" />
        <p class="help-block">Default 0 (order by category name), lighter (lesser) weight is display over heavier weight.</p>
    </div>
</div>
<div class="form-actions">
    <button type="submit" class="btn btn-primary">Save this category</button>
    <button class="btn" onclick="window.location.href='<?php print base_url('admin/categories'); ?>'; return false;">Cancel</button>
</div>
<?php form_close(); ?>
