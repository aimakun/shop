<?php
/**
 * @file item_list.php
 *
 * Products template view for item listing.
 *
 * Available variables:
 * $items: product items array contains some properties:
 * -- id: product id which refer to unique product.
 * -- title: product name.
 * -- detail: product details eg. specifications, some product reviews, etc.
 * -- status: sale status for this product (available, sold_out, pre_order, promotion.)
 * -- price: product price.
 * -- price_sale: special product price which would be used on promotion products.
 * -- stock: current available pieces of this product.
 * -- stock_sold: current sold out pieces of this product.
 * -- created_at / updated_at: product information create / update date.
 * -- category_id: refer to this product's category.
 * -- attachment_id: refer to this product's attachment (eg. cover image, review photo).
 * -- attribute: attribute item array which contains:
 * ---- label: attribute description shown in product page.
 * ---- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 * 
 * Note for developers:
 * Collect product items with foreach() loop and create output as example below:
 * foreach ($items as $product_item):
 *   print '<a href="/products/view/' . $product_item->id . '">' . $product_item->title . '</a>';
 * endforeach;
 * 
 */
?>
<div class="item-list">
    <?php foreach ($items as $product_item): ?>
    <div class="item span3 clearfix<?php print $product_item->stock - $product_item->stock_sold < 1 ? ' alert-danger' : ''; ?>">
        <a class="attachment" href="<?php print base_url('products/view/' . $product_item->id); ?>">
            <?php if (isset($product_item->attachment->path)): ?>
                <img src="<?php print base_url($product_item->attachment->path); ?>"
                    alt="Image of <?php print $product_item->title; ?>" />
            <?php endif; ?>
        </a>
        
        <a class="title" href="<?php print base_url('products/view/' . $product_item->id); ?>">
            <?php print $product_item->title; ?>
        </a>
        <div class="sale-metadata">
            <div class="price">
                <?php if ((int) $product_item->price_sale > 0): ?>
                    <span class="sale"><?php print number_format($product_item->price); ?></span>
                    <span><?php print number_format($product_item->price_sale); ?></span> บาท</div>
                <?php else: ?>
                    <span><?php print number_format($product_item->price); ?></span> บาท</div>
                <?php endif; ?>
            <span class="status status-<?php print $product_item->status; ?>">(<?php print $product_item->status; ?>)</span>
            <?php if ($product_item->stock - $product_item->stock_sold < 1): ?>
            <span class="label label-important">sold out</span>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
</div>
