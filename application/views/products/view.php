<?php
/**
 * @file view.php
 *
 * Products template view for product page.
 *
 * Available variables:
 * $product: product item contains some properties:
 * -- id: product id which refer to unique product.
 * -- title: product name.
 * -- detail: product details eg. specifications, some product reviews, etc.
 * -- status: sale status for this product (available, sold_out, pre_order, promotion.)
 * -- price: product price.
 * -- price_sale: special product price which would be used on promotion products.
 * -- stock: current available items of this product.
 * -- stock_sold: current sold out items of this product.
 * -- created_at / updated_at: product information create / update date.
 * -- category_id: refer to this product's category.
 * -- attachment_id: refer to this product's attachment (eg. cover image, review photo).
 * -- attribute: attribute item array which contains:
 * ---- label: attribute description shown in product page.
 * ---- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 * 
 * $available_percent: processed available items in stock to percentage.
 * $progress_classname: processed bootstrap's progress bar class name, affect in bar color.
 * 
 */
?>
<div class="product">
    <div class="attachment-wrapper span4">
        <?php if (isset($product->attachment->path)): ?>
            <img class="attachment" src="<?php print base_url($product->attachment->path); ?>"
                alt="Image of <?php print $product->title; ?>" />
        <?php endif; ?>
    </div>
    <div class="metadata span5 well">
        <h3 class="title"><?php print $product->title; ?></h3>
        <p class="status status-<?php print $product->status; ?>">(<?php print $product->status; ?>)</p>
        <?php if (count($product->attribute) > 0): ?>
        <ul class="attributes">
            <?php foreach ($product->attribute as $product_attribute): ?>
            <li class="attribute">
                <?php if ($product_attribute->label != '<none>'): ?>
                <h4><?php print htmlspecialchars($product_attribute->label); ?></h4>
                <?php endif; ?>
                <p><?php print htmlspecialchars($product_attribute->notes); ?></p>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        <p class="price">ราคา 
            <?php if ((int) $product->price_sale > 0): ?>
                <span class="sale"><?php print number_format($product->price); ?></span>
                <span><?php print number_format($product->price_sale); ?></span> บาท
            <?php else: ?>
                <span><?php print number_format($product->price); ?></span> บาท
            <?php endif; ?>
        </p>
        
        <?php if ($product->stock - $product->stock_sold < 1): ?>
            <div class="alert alert-error">
                ขออภัยสินค้าหมดชั่วคราว
            </div>
        <?php else: ?>
            <p class="order-action">
                <a href="<?php print base_url('orders/add/' . $product->id); ?>" class="btn btn-primary">Order this product</a>
            </p>
            <p class="sold-label">สินค้าพร้อมส่งคงเหลือ</p>
            <div class="sold-bar span2 progress progress-striped active<?php print $progress_classname; ?>">
                <div class="bar" style="width: <?php print $available_percent; ?>%;"><?php print $available_percent; ?>%</div>
            </div>
        <?php endif; ?>
    </div>
    <div class="detail span9">
        <?php print $product->detail; ?>
    </div>
</div>
