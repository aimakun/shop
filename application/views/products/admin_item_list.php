<?php
/**
 * @file admin_item_list.php
 *
 * Products template view for item listing (backend).
 *
 * Available variables:
 * $items: product items array contains some properties:
 * -- id: product id which refer to unique product.
 * -- title: product name.
 * -- detail: product details eg. specifications, some product reviews, etc.
 * -- status: sale status for this product (available, sold_out, pre_order, promotion.)
 * -- price: product price.
 * -- price_sale: special product price which would be used on promotion products.
 * -- stock: current available pieces of this product.
 * -- stock_sold: current sold out pieces of this product.
 * -- created_at / updated_at: product information create / update date.
 * -- category_id: refer to this product's category.
 * -- attachment_id: refer to this product's attachment (eg. cover image, review photo).
 * -- attribute: attribute item array which contains:
 * ---- label: attribute description shown in product page.
 * ---- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 * 
 * Note for developers:
 * Collect product items with foreach() loop and create output as example below:
 * foreach ($items as $product_item):
 *   print '<a href="/products/view/' . $product_item->id . '">' . $product_item->title . '</a>';
 * endforeach;
 * 
 */
?>
<a href="<?php print base_url('admin/add/product'); ?>" class="btn btn-large btn-primary"><i class="icon-plus icon-white"></i> New product</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Category</th>
            <th>Title</th>
            <th>Updated</th>
            <th>Remaining</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $product_item): ?>
        <tr>
            <td><?php print $product_item->id; ?></td>
            <td><i class="icon-tag"></i> <?php print $product_item->category->title; ?></td>
            <td><?php print $product_item->title; ?></td>
            <td><i class="icon-calendar"></i> <?php print $product_item->updated_at->format('j F Y H:i'); ?></td>
            <td>
                <?php
                if ($product_item->stock != 0) {
                    $available_percent = floor((1 - $product_item->stock_sold / $product_item->stock) * 100);
                }
                else { // Unknown stock, fallback with 0.
                    $available_percent = 0;
                }
                if ($available_percent < 20) {
                    $progress_classname = ' label-danger';
                }
                else if ($available_percent < 50) {
                    $progress_classname = ' label-warning';
                }
                else {
                    $progress_classname = ' label-success';
                }
                ?>
            <!-- <span class="label <?php print $progress_classname; ?>"><?php print $available_percent; ?>%</span>--> <?php print number_format($product_item->stock - $product_item->stock_sold); ?> ชิ้น
            </td>
            <td>
                <a href="<?php print base_url('admin/delete/product/' . $product_item->id); ?>" onclick="return confirm('Are you sure to delete?');"><i class="icon-trash"></i> Delete</a> | 
                <a href="<?php print base_url('admin/edit/product/' . $product_item->id); ?>"><i class="icon-edit"></i> Edit</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
