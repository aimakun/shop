<?php
/**
 * @file admin_edit.php
 *
 * Backend - products edit form.
 *
 * Available variables:
 * $product: product item contains some properties:
 * -- id: product id which refer to unique product.
 * -- title: product name.
 * -- detail: product details eg. specifications, some product reviews, etc.
 * -- status: sale status for this product (available, sold_out, pre_order, promotion.)
 * -- price: product price.
 * -- price_sale: special product price which would be used on promotion products.
 * -- stock: current available items of this product.
 * -- stock_sold: current sold out items of this product.
 * -- created_at / updated_at: product information create / update date.
 * -- category_id: refer to this product's category.
 * -- attachment_id: refer to this product's attachment (eg. cover image, review photo).
 * -- attribute: attribute item array which contains:
 * ---- label: attribute description shown in product page.
 * ---- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 * $selections: form data for generate some select elements:
 * -- status: product status
 * -- categories: array of category objects, contains category_id and title.
 * $file_error: Error message from File Uploading class.
 */

$error = validation_errors() . $file_error;
if (!empty($error)): ?>
<div class="alert alert-error">
    <?php print $error; ?>
</div>
<?php endif; ?>
<?php print form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="title">Title</label>
    <div class="controls">
        <input type="text" id="title" name="title" value="<?php print set_value('title', $product->title); ?>" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="attachment">
        Attachment
    </label>
    <div class="controls">
        <?php if (isset($product->attachment->path)): ?>
        <div class="attachment-view">
            <img src="<?php print base_url($product->attachment->path); ?>" />
        </div>
        <label for="attachment_clear">
            <?php print form_checkbox(array('name' => 'attachment_clear',  'value' => '1', 'id' => 'attachment_clear')); ?>
            Clear attachment
        </label>
        <?php endif; ?>
        <?php print form_upload('attachment'); ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="detail">
        Detail
    </label>
    <div class="controls">
        <textarea name="detail" id="detail" class="tinymce" rows="20" cols="200">
<?php print set_value('detail', $product->detail); ?>
        </textarea>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="category_id">
        Category
    </label>
    <div class="controls">
        <select name="category_id" id="category_id">
            <option value="0">-- Please select --</option>
            <?php foreach ($selections['categories'] as $category): ?>
            <option value="<?php print $category->id; ?>" <?php print isset($product->category->id) && $category->id == $product->category->id ? 'selected="selected"' : ''; ?>><?php print $category->title; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="attributes">
        Attributes
    </label>
    <div class="controls">
        <?php if (empty($product->id)): ?>
            <p class="help-block">Save this product first, so you can setup this product attributes.</p>
        <?php else: ?>
            <?php if (count($product->attribute) > 0): ?>
        <table class="table table-striped table-bordered table-condensed">
            <tbody>
                <?php foreach ($product->attribute as $product_attribute): ?>
                <tr>
                    <td class="product-attribute-label"><?php print htmlspecialchars($product_attribute->label); ?></td>
                    <td><?php print  htmlspecialchars($product_attribute->notes); ?></td>
                    <td>
                        <a href="<?php print base_url('admin/delete/attribute/' . $product_attribute->id); ?>" onclick="return confirm('Are you sure to delete?');"><i class="icon-trash"></i> Delete</a> | 
                        <a href="<?php print base_url('admin/edit/attribute/' . $product_attribute->id); ?>"><i class="icon-edit"></i> Edit</a></td>
                    </td>
                </tr>
                <?php endforeach; ?>
        </tbody>
        </table>
            <?php endif; ?>
        <a href="<?php print base_url('admin/add/attribute/' . $product->id); ?>" class="btn"><i class="icon-plus"></i> New attribute</a>
        <p class="help-block">Create or edit any attributes cause lose any unsaved changes, save any changes in this product before continue.</p>
        <?php endif; ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="status">
        Status
    </label>
    <div class="controls">
        <select id="status" name="status">
            <?php foreach ($selections['status'] as $status_type): ?>
            <option value="<?php print $status_type; ?>" <?php print $status_type == $product->status ? 'selected="selected"' : ''; ?>><?php print $status_type; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="price">Price</label>
    <div class="controls">
        <input type="text" id="price" name="price" value="<?php print set_value('price', $product->price); ?>" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="price_sale">Sale price</label>
    <div class="controls">
        <input type="text" id="price_sale" name="price_sale" value="<?php print set_value('price_sale', $product->price_sale); ?>" />
        <p class="help-block">Set for products which in promotion.</p>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="stock">Total stock</label>
    <div class="controls">
        <input type="text" id="stock" name="stock" value="<?php print set_value('stock', $product->stock); ?>" />
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="stock_sold">Total sold</label>
    <div class="controls">
        <input type="text" id="stock_sold" name="stock_sold" value="<?php print set_value('stock_sold', $product->stock_sold); ?>" />
    </div>
</div>
<div class="form-actions">
    <button type="submit" class="btn btn-primary">Save this product</button>
    <button class="btn" onclick="window.location.href='<?php print base_url('admin/products'); ?>'; return false;">Cancel</button>
</div>
<?php form_close(); ?>
