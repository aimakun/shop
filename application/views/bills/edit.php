<?php
/**
 * @file edit.php
 *
 * Template view for the bill page form.
 * 
 * Available variables:
 * $bill: bill information contains:
 * -- amount: transfer amount for product orders.
 * -- bank: bank gateway which member transfer to.
 * -- shipping_type: the type of shipping (normal, ems).
 * -- shipping_address: shipping destination which orders would send to.
 * -- status: billing status (wait, checking, approve, deadline, problem, reserve, refund.)
 * -- created_at / updated_at: bill information create / update date.
 * 
 * -- order: order items array contains some properties:
 * ---- id: order id, for anonymous it would be temporary id reference to current session.
 * ---- product: product items array like $product, but reference with current member orders.
 * ------ id: product id which refer to unique product.
 * ------ title: product name.
 * ------ detail: product details eg. specifications, some product reviews, etc.
 * ------ status: sale status for this product (available, sold_out, pre_order, promotion.)
 * ------ price: product price.
 * ------ price_sale: special product price which would be used on promotion products.
 * ------ stock: current available pieces of this product.
 * ------ stock_sold: current sold out pieces of this product.
 * ------ created_at / updated_at: product information create / update date.
 * ------ category_id: refer to this product's category.
 * ------ attachment_id: refer to this product's attachment (eg. cover image, review photo).
 * ------ attribute: attribute item array which contains:
 * -------- label: attribute description shown in product page.
 * -------- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 */
?>
<?
$error = validation_errors();
if (!empty($error)): ?>
<div class="alert alert-error">
    <?php print $error; ?>
</div>
<?php endif; ?>
<?php print form_open(current_url(), array('class' => 'well form-horizontal')); ?>
<fieldset>
    <legend>ยืนยันการโอนเงิน</legend>
    <div class="control-group">
        <label class="control-label" for="amount">Amount</label>
        <div class="controls">
            <input type="text" id="amount" name="amount" value="<?php print set_value('amount', $bill->amount); ?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="bank">
            Bank
        </label>
        <div class="controls">
            <select  class="span2" id="bank" name="bank">
                <?php
                foreach ($banks as $bank): 
                    if (!$this->input->post('province'))
                    {
                        $is_default = $bill->bank === $bank;
                    }
                    else
                    {
                        $is_default = $this->input->post('bank') === $bank;
                    }
                ?>
                <option value="<?php print $bank; ?>" <?php print $is_default ? 'selected="selected"' : ''; ?>><?php print $bank; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="transfer_date">Transfer on</label>
        <div class="controls">
            <input type="text" class="span1" id="transfer_day" name="transfer_day" value="<?php
                if (!$this->input->post('transfer_day'))
                {
                    print set_value('transfer_day', $transfer_date['day']);
                }
                else
                {
                    print set_value('transfer_day', $this->input->post('transfer_day'));
                }
            ?>" />
            <select class="span2" id="transfer_month" name="transfer_month">
                <?php 
                $month = array(1 => 'January',
                'February', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December');
                for ($i = 1; $i <= 12; $i++): 
                    if (!$this->input->post('transfer_month'))
                    {
                        $is_default = (int) $transfer_date['month'] === $i;
                    }
                    else
                    {
                        $is_default = (int) $this->input->post('transfer_month') === $i;
                    }
                    ?>
                    <option value="<?php print $i; ?>" <?php echo set_select('transfer_month', $i, $is_default); ?>>
                        <?php print $month[$i]; ?>
                    </option>
                <?php endfor; ?>
            </select>
            <input type="text" class="span1" id="transfer_year" name="transfer_year" value="<?php
                if (!$this->input->post('transfer_year'))
                {
                    print set_value('transfer_year', $transfer_date['year']);
                }
                else
                {
                    print set_value('transfer_year', $this->input->post('transfer_year'));
                }
            ?>" />
            <p class="help-block">Example: 10 Jan 1990.</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="transfer_time">Transfer at</label>
        <div class="controls">
            <input type="text" id="transfer_time" name="transfer_time" value="<?php print set_value('transfer_time', $bill->transfer_time); ?>" />
            <p class="help-block">Example: 08:05, 12:20.</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="shipping_type">Shipping type</label>
        <div class="controls">
            <label>
            <input type="radio" id="shipping_type_normal" name="shipping_type" value="normal" 
            <?php 
             if ((!$this->input->post('shipping_type') && $bill->shipping_type == 'normal') || 
                $this->input->post('shipping_type') == 'normal')
            {
                print 'checked="checked"';
            }
            ?> />
                แบบลงทะเบียน
            </label>
        
            <label>
                <input type="radio" id="shipping_type_ems" name="shipping_type" value="ems"
                    <?php 
                     if ((!$this->input->post('shipping_type') && $bill->shipping_type == 'ems') || 
                        $this->input->post('shipping_type') == 'ems')
                    {
                        print 'checked="checked"';
                    }
                    ?> />
                แบบ EMS
            </label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="firstname">First name</label>
        <div class="controls">
            <input type="text" id="firstname" name="firstname" value="<?php print set_value('firstname', $member->firstname); ?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="lastname">Last name</label>
        <div class="controls">
            <input type="text" id="lastname" name="lastname" value="<?php print set_value('lastname', $member->lastname); ?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="address">
            Address
        </label>
        <div class="controls">
            <textarea class="span4" name="address" id="address" rows="10" cols="50">
<?php
if (!$this->input->post('address'))
{
    print set_value('address', $member->address); 
}
else
{
    print set_value('address', $this->input->post('address')); 
}
?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="province">
            Province
        </label>
        <div class="controls">
            <select  class="span2" id="province" name="province">
                <?php
                foreach ($provinces as $id => $province): 
                    if (!$this->input->post('province'))
                    {
                        $is_default = $member->province === $province;
                    }
                    else
                    {
                        $is_default = $this->input->post('province') == $id;
                    }
                ?>
                <option value="<?php print $id; ?>" <?php print $is_default ? 'selected="selected"' : ''; ?>><?php print $province; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="zipcode">Zip code</label>
        <div class="controls">
            <input type="text" class="span1" id="zipcode" name="zipcode" maxlength="5" value="<?php 
                if (!$this->input->post('zipcode'))
                {
                    print set_value('zipcode', $member->zipcode);
                }
                else
                {
                    print set_value('zipcode', $this->input->post('zipcode'));
                }
                ?>" />
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-primary">ยืนยันการโอนเงิน</button>
        <button class="btn" onclick="window.location.href='<?php print $this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : ''; ?>'; return false;">Cancel</button>
    </div>
</fieldset>
<?php form_close(); ?>
