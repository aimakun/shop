<?php
/**
 * @file admin_edit.php
 *
 * Backend - attribute edit form.
 *
 * Available variables:
 * $attribute: attribute item contains some properties:
 * -- id: attribute id which refer to unique attribute.
 * -- product_id: product id which have this attribute.
 * -- label: attribute description shown in product page.
 * -- notes: attribute notes, eg. next lot / pre-order date for specific attribute.
 */

$error = validation_errors();
if (!empty($error)): ?>
<div class="alert alert-error">
    <?php print $error; ?>
</div>
<?php endif; ?>
<?php print form_open_multipart(current_url(), array('class' => 'form-horizontal')); ?>
<div class="control-group">
    <label class="control-label" for="label">Label</label>
    <div class="controls">
        <input type="text" id="label" name="label" value="<?php print set_value('title', $attribute->label); ?>" />
        <p class="help-block">Use &lt;none&gt; for create only note, member would not select this attribute when order this product.</p>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="notes">
        Notes
    </label>
    <div class="controls">
        <textarea name="notes" id="notes" rows="3" cols="30">
<?php print set_value('notes', $attribute->notes); ?></textarea>
    </div>
</div>
<div class="form-actions">
    <button type="submit" class="btn btn-primary">Save this attribute</button>
    <button class="btn" onclick="window.location.href='<?php print base_url('admin/edit/product/' . $attribute->product_id); ?>'; return false;">Cancel</button>
</div>
<?php form_close(); ?>
