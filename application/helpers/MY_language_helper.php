<?php
/**
 * @file: MY_language_helper.php
 * Override lang() function with supported formatted variables.
 * 
 * @param string  name in $lang configuration.
 * @param string  target id, this function would create <label> tag for target HTML element.
 * @param array  formatted variables as in PHP's printf, collected in array.
 */
function lang($line, $id = '', $args = array())
{
    $CI =& get_instance();
    $line = $CI->lang->line($line);
    
    if (count($args) > 0)
    {
        $line = vsprintf($line, $args);
    }

    if ($id != '')
    {
        $line = '<label for="'.$id.'">'.$line."</label>";
    }

    return $line;
}
