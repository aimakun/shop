<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Note: Use MY_Controller instead of CI_Controller to use global frontend page template.
class Products extends MY_Controller {
    public function index()
    {
        $output = $this->init_frontend_page('Products');
        
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = $this->load->view('products/item_list',
            array(
                'items' => Product::find(
                    'all', 
                    array('order' => 'created_at desc')
                )
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function category($category_id)
    {
        // Get category information.
        $category = Category::find($category_id);
        if (!$category) {
            redirect('product');
        }
        
        $output = $this->init_frontend_page('Products - ' . $category->title);
        
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = $this->load->view('products/item_list',
            array(
                'items' => Product::find(
                    'all', 
                    array('order' => 'created_at desc', 'conditions' => 'category_id =' . (int) $category_id)
                )
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function view($product_id)
    {
        try {
            $product = Product::find((int) $product_id);
        }
        catch (Exception $e) {
            show_404(uri_string(current_url()));
        }
        
        if ($product) {
            $output = $this->init_frontend_page($product->title);
        
            // Add available items in percentage, use bootstrap progress bar conventions.
            if ($product->stock > 0) {
                $available_percent = floor((1 - $product->stock_sold / $product->stock) * 100);
            }
            else {
                $available_percent = 0;
            }
            if ($available_percent < 20) {
                $progress_classname = ' progress-danger';
            }
            else if ($available_percent < 50) {
                $progress_classname = ' progress-warning';
            }
            else {
                $progress_classname = ' progress-success';
            }
            
            // Frontend template: IMPORTANT - our page contents are here.
            $output['content'] = $this->load->view('products/view',
                array(
                    'product' => $product,
                    'available_percent' => $available_percent,
                    'progress_classname' => $progress_classname,
                )
            , TRUE);
        }
        else {
            $output = $this->init_frontend_page('Unknown product');
        }
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
}
