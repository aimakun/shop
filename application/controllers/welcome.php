<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Note: Use MY_Controller instead of CI_Controller to use global frontend page template.
class Welcome extends MY_Controller {
    public function index()
    {
        $output = $this->init_frontend_page();
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = 'Welcome to our homepage!';
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
}
