<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Note: Use MY_Controller instead of CI_Controller to use global frontend page template.
class Orders extends MY_Controller {
    public function index()
    {
        // No need to use now, redirect to homepage.
        redirect('');
    }
    
    public function add($product_id)
    {
        if (!is_numeric($product_id))
        {
            $this->session->set_flashdata('status_message', array('error' => 'Invalid product, please try again.'));
            redirect('products');
        }
        
        // Check product availability, find on id need try..catch block because it return Exception when get empty result.
        try
        {
            $product = Product::find($product_id);
        }
        catch (Exception $e)
        {
            $this->session->set_flashdata('status_message', array('error' => 'This product was not found.'));
            redirect('products');
        }
        
        // Anonymous would store their orders in session temporarily.
        if (!$this->session->userdata('email')) 
        {
            if (is_array($this->session->userdata('orders')))
            {
                $this->session->set_userdata(
                    array(
                        'orders' => array_merge(
                            $this->session->userdata('orders'),
                            array($product->id)
                        ),
                    )
                );
            }
            else
            {
                $this->session->set_userdata(
                    array(
                        'orders' => array($product->id)
                    )
                );
            }
            $this->session->set_flashdata('status_message', array('success' => 'New order <em>' . $product->title . '</em> has been created.'));
            redirect('products/view/' . $product->id);
        }
        else
        {
            // Load current member.
            $member = Member::find('all', array('conditions' => array("email='" . $this->session->userdata('email') . "'")));
             
            if (count($member) != 1)
            {
                // Reset failed account, this member may be deleted from admin.
                redirect('members/logout');
            }
            
            $member = $member[0];
            
            $order = new Order();
            $order->member_id = $member->id;
            $order->product_id = $product->id;
            
            if ($order->save())
            {
                $this->session->set_flashdata('status_message', array('success' => 'New order <em>' . $product->title . '</em> has been created.'));
                redirect('products/view/' . $product->id);
            }
            else
            {
                $this->session->set_flashdata('status_message', array('error' => 'Order creation failed, please try again. (This may be some technical issues.)'));
                redirect('products/view/' . $product->id);
            }
        }
    }
    
    public function delete($order_id)
    {
        if (!is_numeric($order_id))
        {
            $this->session->set_flashdata('status_message', array('error' => 'Invalid order, please try again.'));
            redirect($this->session->flashdata('referrral') ? $this->session->flashdata('referrral') : 'products');
        }
        
        if (!$this->session->userdata('email')) 
        {
            if (is_array($this->session->userdata('orders')))
            {
                $current_orders = $this->session->userdata('orders');
                unset($current_orders[$order_id]);
                
                $this->session->set_userdata(
                    array(
                        'orders' => $current_orders
                    )
                );
            }
            $this->session->set_flashdata('status_message', array('success' => 'Cancel order successfully.'));
            redirect($this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'products');
        }
        else
        {
            // Load current member.
            $member = Member::get_current_profile();
            
            try
            {
                $removed_order = Order::find($order_id);
            }
            catch (Exception $e)
            {
                $this->session->set_flashdata('status_message', array('error' => 'Invalid order, please try again.'));
                redirect($this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'products');
            }
            
            $removed_order->delete();
            $this->session->set_flashdata('status_message', array('success' => 'Cancel order successfully.'));
            redirect($this->session->flashdata('referrer') ? $this->session->flashdata('referrer') : 'products');
        }
    }

    public function bill()
    {
        // Bill information needs authenticated account, move visitor to log in / register page.
        $member = Member::get_current_profile();
        
        // Show billing history
        $bills_history = Bill::find('all', 
            array(
                'conditions' => 'member_id = ' . $member->id,
            )
        );
        
        // List waiting orders with summary in EMS / Normal shipping.
        // Temporary fixed value for shipping cost.
        $shipping_costs = array('normal' => 60, 'ems' =>  90);
        $orders = Order::get_current_orders();
        
        $orders_list = $this->load->view('orders/waiting_list',
            array(
                'orders' => $orders,
                'shipping_costs' => $shipping_costs,
            )
        , TRUE);
        
        $output = $this->init_frontend_page('My bill', $show_my_orders = FALSE);
        
        // Frontend template: IMPORTANT - our page contents are here.
        // Note: this would show only before user post form, or some validations failed.
        // Otherwise page would redirect with above logic.
        
        if (count($orders) > 0) {
            $form = Bill::create_form(new Bill());
        }
        else {
            $form = '<div class="alert alert-info">ไม่มีรายการสินค้าที่สั่งซื้อและยังไม่ได้แจ้งโอนเงิน</div>';
        }
        
        $output['content'] = $this->load->view('orders/bill',
            array(
                'orders_list' => $orders_list,
                'bill_form' => $form,
                'bills_history' => $bills_history,
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function validate_shipping_type($shipping_type)
    {
        $shipping_types = Bill::get_enum_values('shipping_type');
        if (in_array($shipping_type, $shipping_types))
        {
            return TRUE;
        }
        
        $this->form_validation->set_message('validate_shipping_type', 'The Shipping type field is invalid value.');
        return FALSE;
    }
    
    public function validate_bank($bank_name)
    {
        $banks = Bill::get_enum_values('bank');
        if (in_array($bank_name, $banks))
        {
            return TRUE;
        }
        
        $this->form_validation->set_message('validate_bank', 'The Bank field is required.');
        return FALSE;
    }
    
    public function validate_province($province_id)
    {
        $provinces = Member::get_provinces();
        if ($province_id > 0 && isset($provinces[$province_id]))
        {
            return TRUE;
        }
        
        $this->form_validation->set_message('validate_province', 'The Province field is invalid value.');
        return FALSE;
    }
    
    public function validate_transfer_date($transfer_day)
    {
        $transfer_day = str_pad($transfer_day, 2, '0', STR_PAD_LEFT);
        $transfer_month = str_pad((int) $this->input->post('transfer_month'), 2, '0', STR_PAD_LEFT);
        $transfer_year = str_pad((int) $this->input->post('transfer_year'), 4, '0', STR_PAD_LEFT);
        
        if ($transfer_year < 2012 || $transfer_year > 2100)
        {
            $this->form_validation->set_message('validate_birthdate', 'The Transfter field is invalid year.');
            return FALSE;
        }
        if ($transfer_month < 1 || $transfer_month > 12)
        {
            $this->form_validation->set_message('validate_birthdate', 'The Transfter field is invalid month.');
            return FALSE;
        }
        
        $transfer_date = $transfer_year . '-' . $transfer_month . '-' . $transfer_day;
        
        if ($transfer_date != date('Y-m-d', strtotime($transfer_date)))
        {
            $this->form_validation->set_message('validate_transfer_date', 'The Transfter date field is invalid day.');
            return FALSE;
        }
        
        return TRUE;
    }
}
