<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Note: Use MY_Controller instead of CI_Controller to use global frontend page template.
class Admin extends MY_Controller {
    public function index()
    {
        $this->_set_backend_permission();
        
        $output = $this->init_backend_page('Dashboard');
        
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = '<p>Welcome to backend dashboard.</p>';
        $output['content'] .= '<ul>
        <li><a href="' . base_url('admin/products') .'">Products</a></li>
        <li><a href="' . base_url('admin/categories') .'">Categories</a></li>
</ul>';
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('backend/page', $output);
    }
    
    public function products()
    {
        $this->_set_backend_permission();
        
        $output = $this->init_backend_page('Manage products');
        
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = $this->load->view('products/admin_item_list',
            array(
                'items' => Product::find(
                    'all', 
                    array('order' => 'created_at desc')
                )
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('backend/page', $output);
    }
    
    public function categories()
    {
        $this->_set_backend_permission();
        
        $output = $this->init_backend_page('Manage categories');
        
        // Frontend template: IMPORTANT - our page contents are here.
        $output['content'] = $this->load->view('category/admin_item_list',
            array(
                'items' => Category::find(
                    'all', 
                    array('order' => 'weight, title')
                )
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('backend/page', $output);
    }
    
    /**
     * Edit content page.
     */
    public function edit($content_type, $content_id)
    {
        $this->_set_backend_permission();
        
        if (!is_numeric($content_id) || !in_array($content_type, array('product', 'category', 'attribute')))
        {
            $this->session->set_flashdata('status_message', array('error' => 'Incorrect action, please try again.'));
            redirect('admin');
        }
        
        $this->load->library('form_validation');
        
        switch ($content_type)
        {
            case 'product':
                $this->_set_product_validation_rules();
                
                $product = Product::find($content_id);
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($product->$field))
                        {
                            $product->$field = $value;
                        }
                    }
                    
                    // Check upload file is available (user-upload).
                    if ($_FILES['attachment']['size'] > 0)
                    {
                        $upload_config = array(
                            'upload_path' => './static/img/attachments/',
                            'allowed_types' => 'bmp|gif|jpg|png',
                            'max_size' => '30000',
                        );
                            
                        $this->load->library('upload', 
                            $upload_config
                        );
                    
                        if (! $this->upload->do_upload('attachment'))
                        {
                            $file_error = $this->upload->display_errors();
                        }
                        else {
                            $upload_metadata = $this->upload->data();
                            $attachment = new Attachment(
                                array(
                                    'path' => 'static/img/attachments/' . $upload_metadata['file_name']
                                )
                            );
                            $attachment->save();
                            
                            // Remove previous file, it would be replaced by the new one.
                            if ($product->attachment_id > 0) {
                                try
                                {
                                    $removed_attachment = Attachment::find($product->attachment_id);
                                    unlink(str_replace(base_url(), '', $removed_attachment->path)); // Revert absolute path to relative one.
                                    $removed_attachment->delete();
                                }
                                catch (Exception $e)
                                {
                                    // When Attachment Model was not found attachment from id, leave it alone.
                                    // (Prevent errors from manually truncate attachment table.)
                                    // We set try..catch block to handle exception which cause 500 response.
                                    
                                }
                            }
                            
                            $product->attachment_id = $attachment->id;
                        }
                    }
                    else if ($this->input->post('attachment_clear') == 1) {
                        try {
                            $removed_attachment = Attachment::find($product->attachment_id);
                            unlink(str_replace(base_url(), '', $removed_attachment->path)); // Revert absolute path to relative one.
                            $removed_attachment->delete();
                        }
                        catch (Exception $e) {
                            
                        }
                        
                        $product->attachment_id = 0;
                    }
                    
                    // Save then redirect to the list.
                    if (!$file_error) {
                        $product->save();
                        $this->session->set_flashdata('status_message', array('success' => '<em>' . $product->title . '</em> has been updated.'));
                        redirect('admin/products');
                    }
                }
                
                $output = $this->init_backend_page('Edit ' . $product->title);
                
                // Load tinyMCE to textarea field.
                $output['html_preload'] = $this->_load_tinymce();
                
                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('products/admin_edit',
                    array(
                        'product' => $product,
                        'file_error' => isset($file_error) ? $file_error : NULL,
                        'selections' => array(
                            'status' => $this->_get_enum_values('products', 'status'),
                            'categories' => Category::find('all'),
                        )
                    )
                , TRUE);
                break;
            case 'category':
                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('weight', 'Category', 'integer|required');
                
                $category = Category::find($content_id);
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($category->$field))
                        {
                            $category->$field = $value;
                        }
                    }
                    
                    // Save then redirect to the list.
                    $category->save();
                    $this->session->set_flashdata('status_message', array('success' => '<em>' . $category->title . '</em> has been updated.'));
                    redirect('admin/categories');
                }
                
                $output = $this->init_backend_page('Edit ' . $category->title);
                
                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('category/admin_edit',
                    array(
                        'category' => $category,
                    )
                , TRUE);
                break;
            case 'attribute':
                $this->form_validation->set_rules('label', 'Label', 'required');
                
                $attribute = Attribute::find($content_id);
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($attribute->$field))
                        {
                            $attribute->$field = $value;
                        }
                    }
                    $this->session->set_flashdata('status_message', array('success' => 'Attribute <em>' . htmlspecialchars($attribute->label) . '</em> has been updated.'));
                    
                    // Save then redirect to the list.
                    $attribute->save();
                    try
                    {
                        $refer_product = Product::find($attribute->product_id);
                        $refer_pruduct->save(); // Set product lastest update date.
                    }
                    catch (Exception $e)
                    {
                        $this->session->set_flashdata('status_message', array('error' => 'Product ' . $reference_id . ' is not exists, so you cannot create attribute in this one.'));
                        redirect('admin/products');
                    }
                    
                    redirect('admin/edit/product/' . $attribute->product_id);
                }
                
                $output = $this->init_backend_page('Edit attribute <em>' . htmlspecialchars($attribute->label) . '</em>.');

                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('attribute/admin_edit',
                    array(
                        'attribute' => $attribute,
                    )
                , TRUE);
                break;
        }
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('backend/page', $output);
    }
    
    /**
     * Add content page.
     * This action is like edit page, the difference is it loads with the new content object instead of find existing one.
     */
    public function add($content_type, $reference_id = 0)
    {
        $this->_set_backend_permission();
        
        if (!in_array($content_type, array('product', 'category', 'attribute')))
        {
            $this->session->set_flashdata('status_message', array('error' => 'Incorrect action, please try again.'));
            redirect('admin');
        }
        
        $this->load->library('form_validation');
        
        switch ($content_type)
        {
            case 'product':
                $this->_set_product_validation_rules();
                
                $product = new Product(); // Create from new product object.
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($product->$field))
                        {
                            $product->$field = $value;
                        }
                    }
                    
                    // Check upload file is available (user-upload).
                    if ($_FILES['attachment']['size'] > 0)
                    {
                        $upload_config = array(
                            'upload_path' => './static/img/attachments/',
                            'allowed_types' => 'bmp|gif|jpg|png',
                            'max_size' => '30000',
                        );
                            
                        $this->load->library('upload', 
                            $upload_config
                        );
                    
                        if (! $this->upload->do_upload('attachment'))
                        {
                            $file_error = $this->upload->display_errors();
                        }
                        else {
                            $upload_metadata = $this->upload->data();
                            $attachment = new Attachment(
                                array(
                                    'path' => 'static/img/attachments/' . $upload_metadata['file_name']
                                )
                            );
                            $attachment->save();
                            
                            // Remove previous file, it would be replaced by the new one.
                            if ($product->attachment_id > 0) {
                                try
                                {
                                    $removed_attachment = Attachment::find($product->attachment_id);
                                    unlink(str_replace(base_url(), '', $removed_attachment->path)); // Revert absolute path to relative one.
                                    $removed_attachment->delete();
                                }
                                catch (Exception $e)
                                {
                                    // When Attachment Model was not found attachment from id, leave it alone.
                                    // (Prevent errors from manually truncate attachment table.)
                                    // We set try..catch block to handle exception which cause 500 response.
                                    
                                }
                            }
                            
                            $product->attachment_id = $attachment->id;
                        }
                    }
                    else if ($this->input->post('attachment_clear') == 1) {
                        try {
                            $removed_attachment = Attachment::find($product->attachment_id);
                            unlink(str_replace(base_url(), '', $removed_attachment->path)); // Revert absolute path to relative one.
                            $removed_attachment->delete();
                        }
                        catch (Exception $e) {
                            
                        }
                        
                        $product->attachment_id = 0;
                    }
                    
                    // Save then redirect to the list.
                    if (!$file_error) {
                        $product->save();
                        $this->session->set_flashdata('status_message', array('success' => 'Product <em>' . $product->title . '</em> has been created.'));
                        redirect('admin/products');
                    }
                }
                
                $output = $this->init_backend_page('New product');
                
                // Load tinyMCE to textarea field.
                $output['html_preload'] = $this->_load_tinymce();
                
                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('products/admin_edit',
                    array(
                        'product' => $product,
                        'file_error' => isset($file_error) ? $file_error : NULL,
                        'selections' => array(
                            'status' => $this->_get_enum_values('products', 'status'),
                            'categories' => Category::find('all'),
                        )
                    )
                , TRUE);
                break;
            case 'category':
                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('weight', 'Category', 'integer|required');
                
                $category = new Category();  // Create from new product object.
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($category->$field))
                        {
                            $category->$field = $value;
                        }
                    }
                    
                    // Save then redirect to the list.
                    $category->save();
                    $this->session->set_flashdata('status_message', array('success' => 'Category <em>' . $category->title . '</em> has been created.'));
                    redirect('admin/categories');
                }
                
                $output = $this->init_backend_page('New category');
                
                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('category/admin_edit',
                    array(
                        'category' => $category,
                    )
                , TRUE);
                break;
            case 'attribute':
                if ((int) $reference_id < 1)
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Cannot create attribute without product information.'));
                    redirect('admin/products');
                }
                
                // Check product availability.
                try
                {
                    $refer_product = Product::find($reference_id);
                }
                catch (Exception $e)
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Product ' . $reference_id . ' is not exists, so you cannot create attribute in this one.'));
                    redirect('admin/products');
                }
                
                $this->form_validation->set_rules('label', 'Label', 'required');
                
                $attribute = new Attribute();  // Create from new attribute object.
                
                if ($this->form_validation->run())
                {
                    // Normal fields get with foreach loop instead.
                    foreach($this->input->post(NULL, TRUE) as $field => $value)
                    {
                        if (isset($attribute->$field))
                        {
                            $attribute->$field = $value;
                        }
                    }
                    
                    $attribute->product_id = (int) $reference_id;
                    
                    // Save then redirect to the list.
                    $attribute->save();
                    $refer_product->save(); // Set product lastest update date.
                    $this->session->set_flashdata('status_message', array('success' => 'Attribute <em>' . htmlspecialchars($attribute->label) . '</em> has been created.'));
                    redirect('admin/edit/product/' . $reference_id);
                }
                
                $output = $this->init_backend_page('New attribute in product <em>' . $refer_product->title . '</em>');

                // Frontend template: IMPORTANT - our page contents are here.
                $output['content'] = $this->load->view('attribute/admin_edit',
                    array(
                        'attribute' => $attribute,
                    )
                , TRUE);
                break;
        }
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('backend/page', $output);
    }

    public function delete($content_type, $content_id)
    {
        $this->_set_backend_permission();
        
        if (!is_numeric($content_id) || !in_array($content_type, array('product', 'category', 'attachment', 'attribute')))
        {
            $this->session->set_flashdata('status_message', array('error' => 'Incorrect action, please try again.'));
            redirect('admin');
        }
        
        switch ($content_type)
        {
            case 'product':
                $this->_set_product_validation_rules();
                try // Find the product.
                {
                    $product = Product::find($content_id);
                    
                    // Clear the attachment in this product.
                    $product->attachment->delete();
                    
                    // Find the attributes in this product.
                    $attributes_in_product = Attribute::find('all', array('conditions' => 'product_id=' . (int) $content_id));
                    
                    foreach ($attributes_in_product as $attribute)
                    {
                        $attribute->delete();
                    }
                    
                    $this->session->set_flashdata('status_message', array('success' => '<em>' . $product->title . '</em> has been deleted.'));
                    
                    $product->delete();
                    redirect('admin/products');
                }
                catch (Exception $e) // Product not found.
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Product ' . $content_id . ' is not exists.'));
                    redirect('admin');
                }
                break;
            case 'category':
                try // Find the category
                {
                    $category = Category::find($content_id);
                    
                    // Find the products in this category.
                    $products_in_category = Product::find('all', array('conditions' => 'category_id=' . (int) $content_id));
                    
                    foreach ($products_in_category as $product)
                    {
                        // Move to default category.
                        $product->category_id = 1;
                        $product->save();
                    }
                    
                    $this->session->set_flashdata('status_message', array('success' => '<em>' . $category->title . '</em> has been deleted.'));
                    
                    $category->delete();
                    redirect('admin/categories');
                }
                catch (Exception $e) // Category not found.
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Category ' . $content_id . ' is not exists.'));
                    redirect('admin');
                }
                break;
            case 'attribute':
                try // Find the attribute
                {
                    $attribute = Attribute::find($content_id);
                    $product_id = $attribute->product_id;
                    $this->session->set_flashdata('status_message', array('success' => '<em>' . htmlspecialchars($attribute->label) . '</em> has been deleted.'));
                    
                    $attribute->delete();
                    
                    try
                    {
                        $refer_product = Product::find($product_id);
                        $refer_pruduct->save(); // Set product lastest update date.
                    }
                    catch (Exception $e)
                    {
                        $this->session->set_flashdata('status_message', array('error' => 'Product ' . $reference_id . ' is not exists, so you cannot create attribute in this one.'));
                        redirect('admin/products');
                    }
                    
                    redirect('admin/edit/product/' . $product_id);
                }
                catch (Exception $e) // Category not found.
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Attribute ' . $content_id . ' is not exists.'));
                    redirect('admin/products');
                }
                break;
        }
    }
    
    private function _set_backend_permission()
    {
        if (!$this->session->userdata('email'))
        {
            $this->session->set_flashdata('status_message', array('error' => 'To access backend, you need to log in with administrator role.'));
            redirect('members/login');
        }
        
        // Load every access to realtime update administration role.
        $member = Member::find('all', array('conditions' => "email = '{$this->session->userdata('email')}'"));
        $member = $member[0];
        
        if ($member->role != 'admin')
        {
            $this->session->set_flashdata('status_message', array('error' => 'Your account could not access backend section.'));
            redirect('');
        }
    }
    
    private function _get_enum_values($table, $field)
    {
        $query = $this->db->query("SHOW COLUMNS FROM $table LIKE '$field'");
        $field_attributes = $query->result();
        
        if (strpos($field_attributes[0]->Type, 'enum') !== 0)
        {
            return FALSE;
        }
        
        // The field type format from MySQL is:
        // [Type] => enum('available','sold_out','pre_order','promotion')
        // So we strip string and cut them to array with substr() and explode() function.
        
        $enum_values = substr($field_attributes[0]->Type, 6, -2);
        $enum_values = explode("','", $enum_values);
        
        return $enum_values;
    }
    
    private function _set_product_validation_rules()
    {
        if (isset($this->form_validation))
        {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('category_id', 'Category', 'integer|greater_than[0]');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('price', 'Price', 'integer|greater_than[0]');
            $this->form_validation->set_rules('price_sale', 'Price', 'integer|greater_than[-1]');
            $this->form_validation->set_rules('stock', 'Total stock', 'integer|greater_than[-1]');
            $this->form_validation->set_rules('stock_sold', 'Total sold', 'integer|greater_than[-1]');
            
            // Check category availability.
            $this->form_validation->set_rules('category_id', 'Category', 'callback_category_check');
        }
    }
    
    // Validation callback: check category availability.
    public function category_check($category_id)
    {
        try
        {
            $category = Category::find((int) $category_id);
        }
        catch (Exception $e) {
            $this->form_validation->set_message('category_check', 'The Category field is required.');
            return FALSE;
        }
        
        return TRUE;
    }
    
    private function _load_tinymce()
    {
        $scripts = '<script src="' . 
                    base_url('static/js/libs/tiny_mce/jquery.tinymce.js') . '"></script>';
        
        $scripts .= "<script>
                $(document).ready( function() {
                    $('textarea.tinymce').tinymce({
                        script_url : '" . base_url('static/js/libs/tiny_mce/tiny_mce.js') . "',
                        // Example content CSS (should be your site CSS)
                        // content_css : '" . base_url('static/css/base.css') . "',

                        mode : 'textareas',
                        theme : 'advanced',
                        plugins : 'pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template',
                        theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect',
                        theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',
                        theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',
                        theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak',
                        theme_advanced_toolbar_location : 'top',
                        theme_advanced_toolbar_align : 'left',
                        theme_advanced_statusbar_location : 'bottom',
                        theme_advanced_resizing : true
                    });
                });
                </script>";
        
        return $scripts;
    }
}
