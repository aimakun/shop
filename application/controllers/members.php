<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Note: Use MY_Controller instead of CI_Controller to use global frontend page template.
class Members extends MY_Controller {
    public function login()
    {
        if ($this->session->userdata('email'))
        {
            redirect('');
        }
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        $output = $this->init_frontend_page('Members login');
        
        if ($this->form_validation->run()) {
            $this->form_validation->set_rules('email', 'Email', 'callback_member_match');
            if ($this->form_validation->run()) {
                $member = Member::find('all', array('conditions' => "email = '{$this->input->post('email')}'"));
                $member = $member[0];
                
                if ($this->session->userdata('orders')) {
                    $orders = $this->session->userdata('orders');
                    foreach ($orders as $product_id) {
                        $order = new Order();
                        $order->member_id = $member->id;
                        $order->product_id = $product_id;
                        $order->save(); // No need to track either they're saved or not.
                    }
                    $this->session->unset_userdata('orders');
                }
                
                $this->session->set_userdata(
                    array(
                        'email' => $member->email,
                        'name' => $member->firstname . ' ' . $member->lastname,
                    )
                );
                
                $this->session->set_flashdata('status_message', array('success' => 'Login successful.'));
                redirect('');
            }
        }
        
        // Frontend template: IMPORTANT - our page contents are here.
        // Note: this would show only before user post form, or some validations failed.
        // Otherwise page would redirect with above logic.
        
        $output['content'] = $this->load->view('members/login',
            ''
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function logout()
    {
        if ($this->session->userdata('email'))
        {
            $this->session->unset_userdata(
                array(
                    'email' => '',
                    'name' => '',
                )
            );
        }
        $this->session->set_flashdata('status_message', array('success' => 'Log out successful.'));
        redirect('');
    }
    
    public function register()
    {
        if ($this->session->userdata('email'))
        {
            redirect('members');
        }
        
        // Create new temporary member, it would be saved after registration done.
        $member = new Member();
        
        $provinces = $member->get_provinces();
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'valid_email|required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('retype_password', 'Retype password', 'matches[password]');
        
        $this->form_validation->set_rules('firstname', 'First name', 'required');
        $this->form_validation->set_rules('lastname', 'Last name', 'required');
        $this->form_validation->set_rules('province', 'Province', 'callback_validate_province');
        $this->form_validation->set_rules('birthdate_day', 'Birthdate (day)', 'callback_validate_birthdate');
        $this->form_validation->set_rules('zipcode', 'ID Zip code', 'numeric|exact_length[5]');
        $this->form_validation->set_rules('phone_tel', 'Tel.', 'numeric');
        $this->form_validation->set_rules('phone_mobile', 'Mobile', 'numeric');
        $this->form_validation->set_rules('id_card', 'ID card', 'numeric|exact_length[13]');
        
        if ($this->form_validation->run())
        {
            $this->form_validation->set_rules('email', 'Email', 'callback_email_not_exists');
            
            if ($this->form_validation->run())
            {
                $post = $this->input->post(NULL, TRUE);
                
                $allow_fields = array(
                    'email', 'firstname', 'lastname', 
                    'id_card', 'gender', 'address', 
                    'zipcode', 'phone_tel', 'phone_mobile'
                );
                
                $member->password_hash = sha1($post['password']);
                
                $birthdate_day = str_pad((int) $post['birthdate_day'], 2, '0', STR_PAD_LEFT);
                $birthdate_month = str_pad((int) (int) $post['birthdate_month'], 2, '0', STR_PAD_LEFT);
                $birthdate_year = str_pad((int) $post['birthdate_year'], 4, '0', STR_PAD_LEFT);
                
                $member->birthdate = $birthdate_day . '-' . $birthdate_month . '-' . $birthdate_year;
                    
                $member->province = $provinces[$post['province']];
                
                foreach ($allow_fields as $field)
                {
                    if (isset($member->$field))
                    {
                        $member->$field = $post[$field];
                    }
                }
                
                if ($member->save())
                {
                    $this->session->set_userdata(
                        array(
                            'email' => $member->email,
                            'name' => $member->firstname . ' ' . $member->lastname,
                        )
                    );
                    
                    $this->session->set_flashdata('status_message', array('success' => 'Registration successful, now you have logged in with email: <em>' . $member->email . '</em>.'));
                    redirect('members');
                }
                else
                {
                    $this->session->set_flashdata('status_message', array('error' => 'Registration failed, please try again. (This may be some technical issues.)'));
                    redirect('members/register');
                }
            }
        }
        
        $output = $this->init_frontend_page('Register account');
        
        // Frontend template: IMPORTANT - our page contents are here.
        // Note: this would show only before user post form, or some validations failed.
        // Otherwise page would redirect with above logic.
        
        $output['content'] = $this->load->view('members/register',
            array(
                'member' => $member,
                'provinces' => $provinces,
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function edit_profile()
    {
        // Load current member.
        $member = Member::get_current_profile();
        
        $provinces = $member->get_provinces();
        $birthdate = explode('-', $member->birthdate->format('Y-m-d'));
        $birthdate = array(
            'year' => $birthdate[0],
            'month' => $birthdate[1],
            'day' => $birthdate[2]
        );
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('firstname', 'First name', 'required');
        $this->form_validation->set_rules('lastname', 'Last name', 'required');
        $this->form_validation->set_rules('province', 'Province', 'callback_validate_province');
        $this->form_validation->set_rules('birthdate_day', 'Birthdate (day)', 'callback_validate_birthdate');
        $this->form_validation->set_rules('zipcode', 'ID Zip code', 'numeric|exact_length[5]');
        $this->form_validation->set_rules('phone_tel', 'Tel.', 'numeric');
        $this->form_validation->set_rules('phone_mobile', 'Mobile', 'numeric');
        $this->form_validation->set_rules('id_card', 'ID card', 'numeric|exact_length[13]');
        
        $current_password = $this->input->post('current_password');
        if (!empty($current_password))
        {
            $this->form_validation->set_rules('current_password', 'Current password', 'callback_match_current_password');
            $this->form_validation->set_rules('retype_password', 'Retype password', 'matches[password]');
        }
        
        if ($this->form_validation->run())
        {
            $post = $this->input->post(NULL, TRUE);
            
            $allow_fields = array(
                'firstname', 'lastname', 
                'id_card', 'gender', 'address', 
                'zipcode', 'phone_tel', 'phone_mobile'
            );
            
            if (!empty($post['password']))
            {
                $member->password_hash = sha1($post['password']);
            }
            
            $birthdate_day = str_pad((int) $post['birthdate_day'], 2, '0', STR_PAD_LEFT);
            $birthdate_month = str_pad((int) $post['birthdate_month'], 2, '0', STR_PAD_LEFT);
            $birthdate_year = str_pad((int) $post['birthdate_year'], 4, '0', STR_PAD_LEFT);
            
            $member->birthdate = $birthdate_day . '-' . $birthdate_month . '-' . $birthdate_year;
                
            $member->province = $provinces[$post['province']];
            
            foreach ($allow_fields as $field)
            {
                if (isset($member->$field))
                {
                    $member->$field = $post[$field];
                }
            }
            
            if ($member->save())
            {
                $this->session->set_flashdata('status_message', array('success' => 'Account profile ' . $member->email . ' has been updated.'));
                redirect('members/edit_profile');
            }
            else
            {
                $this->session->set_flashdata('status_message', array('error' => 'Profile updates failed, please try again. (This may be some technical issues.)'));
                redirect('members');
            }
        }
        
        $output = $this->init_frontend_page('Edit profile');
        
        // Frontend template: IMPORTANT - our page contents are here.
        // Note: this would show only before user post form, or some validations failed.
        // Otherwise page would redirect with above logic.
        
        $output['content'] = $this->load->view('members/edit',
            array(
                'member' => $member,
                'provinces' => $provinces,
                'birthdate' => $birthdate,
            )
        , TRUE);
        
        // Frontend template: Finally, display overall page with frontend_page.php in views.
        $this->load->view('frontend/page', $output);
    }
    
    public function member_match($email)
    {
        // Check user-input email whether available or not.
        $member = Member::find('all', array('conditions' => "email = '{$email}'"));
        
        if (!$member)
        {
            $this->form_validation->set_message('member_match', 'This email is not registered, ' . anchor('members/register', 'create new account.'));
            return FALSE;
        }
        
        $member = $member[0]; // Need only one row.
        
        // Check this account is actived or blocked.
        if ($member->status == 'blocked')
        {
            $this->form_validation->set_message('member_match', 'This email was blocked, --contact us to ask your solution.--');
            return FALSE;
        }
        
        if ($member->password_hash != sha1($this->input->post('password')))
        {
            $this->form_validation->set_message('member_match', 'Password is not correct, try again or request new password.');
            return FALSE;
        }
        return TRUE;
    }
    
    public function email_not_exists($email)
    {
        // Check user-input email whether available or not.
        $member = Member::find('all', array('conditions' => "email = '{$email}'"));
        
        if (!$member)
        {
            return TRUE;
        }
        
        $this->form_validation->set_message('email_not_exists', 'This email is already registered, please ' . anchor('members/login', 'Log in with your existing account') . '.');
        return FALSE;
    }
    
    public function validate_birthdate($birthdate_day)
    {
        $birthdate_day = str_pad($birthdate_day, 2, '0', STR_PAD_LEFT);
        $birthdate_month = str_pad((int) $this->input->post('birthdate_month'), 2, '0', STR_PAD_LEFT);
        $birthdate_year = str_pad((int) $this->input->post('birthdate_year'), 4, '0', STR_PAD_LEFT);
        
        if ($birthdate_year < 1900 || $birthdate_year > 2100)
        {
            $this->form_validation->set_message('validate_birthdate', 'The Birthdate field is invalid year.');
            return FALSE;
        }
        if ($birthdate_month < 1 || $birthdate_month > 12)
        {
            $this->form_validation->set_message('validate_birthdate', 'The Birthdate field is invalid month.');
            return FALSE;
        }
        
        $birthdate = $birthdate_year . '-' . $birthdate_month . '-' . $birthdate_day;
        
        if ($birthdate != date('Y-m-d', strtotime($birthdate)))
        {
            $this->form_validation->set_message('validate_birthdate', 'The Birthdate field is invalid day.');
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function validate_province($province_id)
    {
        $provinces = Member::get_provinces();
        if ($province_id > 0 && isset($provinces[$province_id]))
        {
            return TRUE;
        }
        
        $this->form_validation->set_message('validate_province', 'The Province field is invalid value.');
        return FALSE;
    }
    
    public function match_current_password($current_password)
    {
        $member = Member::get_current_profile();
        
        if (sha1($current_password) != $member->password_hash)
        {
            $this->form_validation->set_message('match_current_password', 'The Current password field is not match with your account, you couldn\'t change your password before correct it.');
            return FALSE;
        }
        return TRUE;
    }
}
